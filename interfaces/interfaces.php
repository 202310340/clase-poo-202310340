<?php
#creacion de la interface
interface OperacionesGenerales{
#funciones de la interface
    public function Redondeo($monto);
    public function CalcularIVA($monto);
}
#creacion de la clase punto de venta con la implementacion de interfaces
class PuntoDeVenta implements OperacionesGenerales{
#funciones de la clase 
    public function Redondeo($monto){
        return round($monto);
    }
    public function CalcularIVA($monto){
        return $IVA = $monto*.16;
    }
}
?>