<?php 
//creacion de la clase conversiones
class conversiones{
//creamos la funcion privada ConversionRealizar 
    private $ConversionesRealizar;
//creamos el constructor
    function __construct($ConversionesRealizar){
        $this->ConversionesRealizar = $ConversionesRealizar;
    }
//agregamos todas las funciones que vamos a utilizar para los calculos
    private function MY($a){
        return $a * 1.0936;
    }
    private function YM ($a){
        return $a * 0.9144;
    }
    private function MP ($a){
        return $a * 39.3701;
    }
    private function PM ($a){
        return $a * 0.0254;
    }
    private function MC ($a){
        return $a * 100;
    }
    private function CM ($a){
        return $a * 0.01;
    }

    //metodo que ejecuta los metodos en base a la prpiedad $OperacionRealizar 
    public function ResultadoOperacion($a){

        switch ($this->ConversionesRealizar) {
            case 'metros a yardas':
                return $this->MY($a);
                break;
            case 'yardas a metros':
                return $this->YM($a);
                break;
            case 'metros a pulgadas':
                return $this->MP($a);
                break;
            case 'pulgadas a metros':
                return $this->PM($a);
                break;
            case 'metros a centimetros':
                return $this->MC($a);
                break;
            case 'centimetros a metros':
                return $this->CM($a);
                break;
            default:
                return "operacion a relaizar no definida";
                (T_break);
        }

    }

}
if (isset($_POST["operador"])){
    $operador = $_POST["operador"];
    $valor1 = $_POST["valor"];
//instanciamos la clase conversiones creando un objeto
    $obj = new conversiones();
//damos valores a los resultados
    $obj->ConversionesRealizar = $operador;
    $RESULTADO = $obj->ResultadoOperacion($valor1);
//imprimimos los resultados en pantalla
    echo "<h1>El resultado de la Operacion ". $operador." es: ". $RESULTADO;"<h1/>";

    echo '<meta http-equiv="refresh" content="5; url=/05_Conversiones">';
}
?>