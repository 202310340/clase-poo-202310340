<?php
interface Veiculo{
        public function Marca();
        public function Puertas();
        public function Motor();
    }
    class Camioneta implements Veiculo{
        public function Marca(){
            echo "Camioneta<br>";
            echo "La marca es: Ford<br>";
        }
        public function Puertas(){
            echo "Numero de puertas: 2<br>";
        }
        public function Motor(){
            echo "El motor es de: 5.0L<br>";
        }
    }
    class Auto implements Veiculo{ 
        public function Marca(){
            echo "Auto<br>";
            echo "La marca es: Mazda<br>";
        }
         public function Puertas(){
            echo"Numero de puertas: 4<br>";
        }
        public function Motor(){
            echo "El motor es: 2.0L<br>";
        }
    }
$obj=new Camioneta();
$obj->Marca();
$obj->Puertas();
$obj->Motor();
$obj=new Auto();
$obj->Marca();
$obj->Puertas();
$obj->Motor();
?>