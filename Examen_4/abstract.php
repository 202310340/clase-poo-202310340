<?php 
abstract class Ofertas{
    abstract public  function Descuento();
}
class Sudaderas extends Ofertas{
    public function Descuento(){
        echo "20% de descuento en sudaderas<br>";
    }
}
class Pantalones extends Ofertas{
    public function Descuento(){
        echo "10% de descuento en pantalones<br>";
    }
}
$obj=new Sudaderas();
$obj->Descuento();
$obj=new Pantalones();
$obj->Descuento();
?>