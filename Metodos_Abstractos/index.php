<?php
abstract class Transporte{
    abstract public  function Mantenimiento();
}
class Avion extends Transporte{
    public function Mantenimiento(){
        echo "revision de alas<br>";
    }
}
class Automovil extends Transporte{
    public function Mantenimiento(){
        echo "revision de motor<br>";
    }
}
class Ferrocarril extends Transporte{
    public function Mantenimiento(){
        echo "revision de vias<br>";
    }
}
class Barco extends Transporte{
    public function Mantenimiento(){
        echo "revision de timon<br>";
    }
}
class Helicoptero extends Transporte{
    public function Mantenimiento(){
        echo "revision de aspas<br>";
    }
}
$obj=new Avion();
$obj->Mantenimiento();
$obj2=new Automovil();
$obj2->Mantenimiento();
$obj3=new Ferrocarril();
$obj3->Mantenimiento();
$obj3=new Barco();
$obj3->Mantenimiento();
$obj3=new Helicoptero();
$obj3->Mantenimiento();
?>