<?php 


class conversiones{

    public $ConversionesRealizar = "";

    public function MY($a){
        return $a * 1.0936;
    }
    public function YM ($a){
        return $a * 0.9144;
    }
    public function MP ($a){
        return $a * 39.3701;
    }
    public function PM ($a){
        return $a * 0.0254;
    }
    public function MC ($a){
        return $a * 100;
    }
    public function CM ($a){
        return $a * 0.01;
    }

    //metodo que ejecuta los metodos en base a la prpiedad $OperacionRealizar 
    public function ResultadoOperacion($a){

        switch ($this->ConversionesRealizar) {
            case 'metros a yardas':
                return $this->MY($a);
                break;
            case 'yardas a metros':
                return $this->YM($a);
                break;
            case 'metros a pulgadas':
                return $this->MP($a);
                break;
            case 'pulgadas a metros':
                return $this->PM($a);
                break;
            case 'metros a centimetros':
                return $this->MC($a);
                break;
            case 'centimetros a metros':
                return $this->CM($a);
                break;
            default:
                return "operacion a relaizar no definida";
                (T_break);
        }

    }

}

?>