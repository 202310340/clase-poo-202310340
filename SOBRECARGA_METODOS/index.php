<?php
//creacion de la clase
class Operaciones{
//creacion de nuestra funcion
    public function suma(){
        $resultado=0;
        $n = func_num_args();//creamos la funcion para el numero de argumentos
        $parametros=func_get_args();//creamos la funcion para obtener los argumentos
        for ($i=0; $i < $n; $i++) {//ciclo for para que se vayan sumando los elementos
            $resultado+=$parametros[$i];
        }
        return $resultado;
    }
} 
$obj = new Operaciones();//instanciamos nuestra clase
echo $obj->suma(5,4,2,6);//mostramos resultados 
?>
