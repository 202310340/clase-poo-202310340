<?php
//creacion de una clase
class Vehiculo{
//asignacion de atributos y funciones
    protected $Kilometros = 1126548;
//creacion del constructor
    function construct__ ($Kilometros){
        $this->Kilometros = $Kilometros;
    }
    protected function encender(){
        return "Luces encendidas";
    }
    protected function Puerta(){
        return "Puerta cerrada";
    }
    protected function Cofre(){
        return "Cofre abierto";
    }
//creacion de una clase heredada con sus respectivas funciones 
}
class carro extends vehiculo {
    private function setKilometros($kilomtros){
        $this->Kilometros = $Kilometros;
    }
    public function getKilometros(){
        return $this->Kilometros;
    }
    public function encenderLuces(){
        return $this->encender();
    }
    public function CerrarPuerta(){
        return $this->Puerta();
    }
    public function AbrirCofre(){
        return $this->Cofre();
    }

}
//creacion del destructor
function __destruct (){
    $this->Kilometros = PHP_EOL;
}
//imprimir resultados en pantalla
$obj = new carro();
echo "Los kilometros son: ".$obj->getKilometros()." Km"."<br>";
echo $obj->encenderLuces()."<br>";
echo $obj->CerrarPuerta()."<br>";
echo $obj->AbrirCofre()."<br>";

?>