<?php   
//creamos una clase llamada Menu
class Menu{
//creacion de atributos 
    private $precio;
//creacion de un constructor
    function __construct($precio){
        $this->precio = $precio;
    }
//creamos un metodo llamado PrecioPlatillo1
    public function PrecioPlatillo1(){
        return $this->precio;
    }
//creacion del destructor
    function __destruct(){
        $this->precio = PHP_EOL;
    }
}

?>