<?php
//creamos nuestra clase
class Vehiculo{
    //asignamos atributos y funciones a nuestra clase
    protected $Conbustible = 10;
    protected function encender(){
    return "Luces encendidas";
}
//creamos una sub clase con sus respectivas funciones 
}
class Moto extends vehiculo {
    private function setConbustible($Conbustible){
        $this->Conbustible = $Conbustible;
    }
    public function getConbustible(){
        return $this->Conbustible;
    }
    public function encenderLuces(){
        return $this->encender();
    }

}
//por ultimo se mostraran todos los resultados en pantalla
$obj = new Moto();
echo "El conbustible es: ".$obj->getConbustible()."L"."<br>";
echo $obj->encenderLuces();